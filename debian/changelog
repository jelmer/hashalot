hashalot (0.3-7) unstable; urgency=medium

  * Move the VCS from gitorious to gitlab.

 -- Adam Borowski <kilobyte@angband.pl>  Wed, 09 Dec 2015 20:46:25 +0100

hashalot (0.3-6) unstable; urgency=low

  * Document that only the first line of input is hashed, for people who
    are looking for a general purpose hasher.  Closes: #544165.
  * Fix warnings from man.
  * Replace debian/rules with dh.
  * Use DEP-5 copyright format.
  * Drop useless README and ancient NEWS.Debian.
  * Upgrade the VCS to git.
    + ... including Vcs- fields (closes: #720265).
  * Upgrade to policy 3.9.5 (build-{arch,indep}), debhelper 9 (hardening).

 -- Adam Borowski <kilobyte@angband.pl>  Wed, 21 May 2014 20:26:47 +0200

hashalot (0.3-5) unstable; urgency=low

  * Fix segfault or wrong output on long input on RMD160, use all of input
    instead of just the first 128 bytes.  Fix by Luc Maisonobe.
  * Use debhelper level 5, don't ignore errors on "make clean".
  * Drop unused /usr/bin.
  * Add Vcs-Svn and Vcs-Browser.
  * Yank away config.{sub,guess}.
  * Add a watch file.
  * Reverted all autotoolage fixes, they're unused but greatly increase the
    diff's size.
  * Recommends -> Suggests, it has changed meaning.

 -- Adam Borowski <kilobyte@angband.pl>  Fri, 25 Jan 2008 10:26:46 +0100

hashalot (0.3-4) unstable; urgency=low

  * New maintainer.
  * Policy version 3.7.2 (no changes needed).
  * Updated FSF's address.
  * Enabled the testsuite for non-cross builds.

 -- Adam Borowski <kilobyte@angband.pl>  Thu,  1 Jun 2006 05:07:14 +0200

hashalot (0.3-3) unstable; urgency=low

  * Fix dangling manpage symlinks.
    - Closes: #256689

 -- Matthias Urlichs <smurf@debian.org>  Mon, 28 Jun 2004 19:23:30 +0200

hashalot (0.3-2) unstable; urgency=low

  * Don't install manpage in /usr/man. Sigh.
    - Closes: #256224
  * Don't install cryptsetup; that's now a separate Debian package.
    - Relates to #255676

 -- Matthias Urlichs <smurf@debian.org>  Fri, 25 Jun 2004 17:35:03 +0200

hashalot (0.3-1) unstable; urgency=low

  * New Upstream release.
    - New Upstream source location.
    - Upstream now has a manpage (mine ;-)
    - Closes: #254805
  * Added '-q' option to suppress memory lock warning.
    - Closes: #246430 (the part about hashalot being useless to non-root)
      : could be setuid root instead, pending a security check
    - but I am not moving hashalot to /usr/bin
      : compatibility
      : it's dangerous if called casually (binary output)
  * Recommend dmsetup (called by cryptsetup script)
    - Replacing cryptsetup with a "real" program will happen in one of the
      next versions.

 -- Matthias Urlichs <smurf@debian.org>  Fri, 18 Jun 2004 17:22:20 +0200

hashalot (0.2-4) unstable; urgency=medium

  * Move /usr/sbin/hashalot to /usr/bin.
    - Closes: #246430.

 -- Matthias Urlichs <smurf@debian.org>  Wed,  5 May 2004 04:28:43 +0200

hashalot (0.2-3) unstable; urgency=low

  * Include a stub manpage for cryptsetup

 -- Matthias Urlichs <smurf@debian.org>  Sun, 28 Mar 2004 17:35:48 +0200

hashalot (0.2-2) unstable; urgency=low

  * hashalot.c: Erase the passphrase from memory when using a salt
  * Include the cryptsetup.sh script

 -- Matthias Urlichs <smurf@debian.org>  Tue,  9 Mar 2004 13:12:36 +0100

hashalot (0.2-1) unstable; urgency=low

  * Initial Release.                                              Closes:#237036
  * Tell autoconf to use the config.h header and to allow-maintainer-mode.
  * Wrote a small manpage

 -- Matthias Urlichs <smurf@debian.org>  Tue,  9 Mar 2004 12:27:51 +0100

